#!/bin/bash

# Checking the version of Polly CLI
polly --version
# login into Polly with Polly CLI inside a Polly job
polly login --auto

## get the file here from polly CLI

## polly files sync --workspace-id ${WORKSPACE_ID} --source ${FILE_PATH_SOURCE} --destination "./"
polly files copy --workspace-id 7633 --source 'polly://ic-rnaseq.nf' --destination '/script/ic-rnaseq.nf'
polly files sync --workspace-id 7633 --source 'polly://data' --destination '/script/data'

## Run the main program file
./nextflow -log nextflow.log run /script/ic-rnaseq.nf -with-report -with-timeline -with-trace
apt install zip unzip
zip -r work.zip work/
zip -r results.zip results/

#push to workspace
polly files copy --workspace-id 7633 --destination 'polly://nf-out/timeline.html' --source './timeline.html'
polly files copy --workspace-id 7633 --destination 'polly://nf-out/report.html' --source './report.html'
polly files copy --workspace-id 7633 --destination 'polly://nf-out/nextflow.log' --source './nextflow.log'
polly files copy --workspace-id 7633 --destination 'polly://nf-out/trace.txt' --source './trace.txt'
polly files copy --workspace-id 7633 --destination 'polly://nf-out/work.zip' --source './work.zip'
polly files copy --workspace-id 7633 --destination 'polly://nf-out/results.zip' --source './results.zip'

